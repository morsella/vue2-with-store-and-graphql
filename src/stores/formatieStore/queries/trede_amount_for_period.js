import VueResource from 'vue-resource';

const TredeAmountQuery = VueResource('index.html?module=something&action=formatie&salaristrede_id={id}&datum={date}');
export default {
    TredeAmountQuery
}