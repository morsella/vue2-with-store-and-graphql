import Vue from 'vue';
import FormatieApp from './FormatieApp.vue';
import VueResource from 'vue-resource';
import store from './stores/formatieStore/store';
import 'whatwg-fetch';

Vue.use(VueResource);

new Vue({
        el: '#formatie',
        store,
        render: h => h(FormatieApp)
});
