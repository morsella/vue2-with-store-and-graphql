import * as mutations from '../../types/mutations';

//___________________CUSTOM FUNCTIONS______________________________________________________________________________
export default{
	//_________________CONTEXT_____________________________________________________________________________________
    [mutations.UPDATE_CONTEXT]:(state, payload) => {
      state.context = payload;
    },
    [mutations.STORE_UITERSTE_JAAREN]:(state) =>{
        state.allDates = { start: state.context.begin, end: state.context.eind};
    },
    [mutations.UPDATE_JAREN_AND_PERIODES]:(state, payload) =>{
          state.jaren = payload.jaren;
          state.laagste_jaar = state.jaren[0];
          state.hoogste_jaar  = state.jaren.splice(0, -1);
          let array = [];
          for (let period of payload.periodes){
			      let min = period[0];
			      let max = period.slice(-1)[0];
              array.push({min, max});
          }
          state.periodes_array = array;
    },
    //___________PERSONEELSLEDEN____________________________________________________________________________________
    [mutations.TRUNCATE_PERSONEELSLID]:(state) =>{
    	state.personeelsleden = [];
	},
    [mutations.STORE_PERSONEELSLID]:(state, personeelslid) =>{
				let totalen = {};
				let amountPerPeriod = {};
					// totalen[periode.min] = {
					// 	wtf: personeelslid.totaal[0].wtf,
					// 	werkgeverslasten: personeelslid.totaal[0].werkgeverslasten,
					// 	brutoloon: personeelslid.totaal[0].brutoloon,
					// 	bapo: personeelslid.totaal[0].bapo,
					// 	tu: personeelslid.totaal[0].tu,
					// 	maanden: {
					// 		1: {wtf: 0, tu: 0, bapo: 0, werkgeverslasten: 0, brutoloon: 0},
					// 		2: {wtf: 0, tu: 0, bapo: 0, werkgeverslasten: 0, brutoloon: 0},
					// 		3: {wtf: 0, tu: 0, bapo: 0, werkgeverslasten: 0, brutoloon: 0},
					// 		4: {wtf: 0, tu: 0, bapo: 0, werkgeverslasten: 0, brutoloon: 0},
					// 		5: {wtf: 0, tu: 0, bapo: 0, werkgeverslasten: 0, brutoloon: 0},
					// 		6: {wtf: 0, tu: 0, bapo: 0, werkgeverslasten: 0, brutoloon: 0},
					// 		7: {wtf: 0, tu: 0, bapo: 0, werkgeverslasten: 0, brutoloon: 0},
					// 		8: {wtf: 0, tu: 0, bapo: 0, werkgeverslasten: 0, brutoloon: 0},
					// 		9: {wtf: 0, tu: 0, bapo: 0, werkgeverslasten: 0, brutoloon: 0},
					// 		10: {wtf: 0, tu: 0, bapo: 0, werkgeverslasten: 0, brutoloon: 0},
					// 		11: {wtf: 0, tu: 0, bapo: 0, werkgeverslasten: 0, brutoloon: 0},
					// 		12: {wtf: 0, tu: 0, bapo: 0, werkgeverslasten: 0, brutoloon: 0}
					// 	}
					// };
				let personeelslid_data = {
					data: personeelslid,
					betrekking_current: [],
					betrekkingen_total: [],
					functiesCodes:[],
					betrekkingenLoaded: false,
					totalenMaandenLoaded: false,
					showBetrekkingen: false,
					contracten:[],
					detail: {active: false, view: 'personeelslid-details-jaren'},
					edit: {active: false, highlighted: false, masked: false, mask_text: '', error: false}
				};

			state.personeelsleden.push(personeelslid_data);
			// state.loader = false;
    },
    [mutations.STORE_PERSONEEL_TOTALEN_MAANDEN]:(state, {personeelslid_id, totalen}) => {
        let totaal_jaar_maand = state.personeelsleden[state.personeels_map[personeelslid_id]].totalen[totalen.jaar].maanden[totalen.periode];
        totaal_jaar_maand.wtf = totalen.wtf;
        totaal_jaar_maand.tu = totalen.tu;
        totaal_jaar_maand.bapo = totalen.bapo;
        totaal_jaar_maand.werkgeverslasten = totalen.werkgeverslasten;
        totaal_jaar_maand.brutoloon = totalen.brutoloon;
    },
    [mutations.UPDATE_PERSONEELS_MAP]: (state) => {
        state.personeels_map = {};
        state.personeelsleden.forEach(function (item, index) { state.personeels_map[item.data.id] = index; });
    },
    [mutations.UPDATE_PERSONEELSLID_DETAIL_VIEW]: (state, {personeelslid_id, view}) => {
        state.personeelsleden[state.personeels_map[personeelslid_id]].detail.view = view;
    },
	[mutations.AMOUNTS_STORED]:(state, payload) =>{
    	state.amountsPersonelsleden = payload.amounts;
	},
//______________________CONTRACTEN_________________________________________________________________________________
	[mutations.STORE_CONTRACT]:(state, {personeelslid_id, contract}) =>{
		state.personeelsleden[state.personeels_map[personeelslid_id]].contracten.push({
			data: contract,
			betrekkingen: []
		});
	},
	[mutations.LOAD_CONTRACT]:(state) =>{
		// state.personeelsleden[state.personeels_map[personeelslid_id]].contractenLoaded = true;
		state.contracten_map = [];
		state.personeelsleden.forEach(function(person){
			person.contracten.forEach(function(contract, index){
				// person.functiesCodes = person.functiesCodes.concat(contract.data.functiecategorie.code);
				person.functiesCodes.push(contract.data.functiecategorie.code);
				state.contracten_map[contract.data.id] = {personeelslid_id: person.data.id, index: index};
			});
		});
	},
    //_______________________BETREKKINGEN____________________________________________________________________________
    [mutations.STORE_BETREKKING]:(state, {personeelslid_id ,betrekking}) =>{
        state.personeelsleden[state.personeels_map[personeelslid_id]].contracten.forEach(function(contract){
        	if(parseInt(contract.data.id) === parseInt(betrekking.data.contract_id)) {
				contract.betrekkingen.push({
					data: betrekking.data,
					binnen_os_selectie: betrekking.binnen_os_selectie,
					edit: {active: false, highlighted: false, masked: false, mask_text: '', error: false}
				});
			}
		});
    },
	[mutations.TOGGLE_BETREKKINGEN]:(state, personeelslid_id) =>{
		state.personeelsleden[state.personeels_map[personeelslid_id]].showBetrekkingen = !state.personeelsleden[state.personeels_map[personeelslid_id]].showBetrekkingen;
	},
	[mutations.LOAD_BETREKKINGEN]:(state) =>{
		let checkBretekking = function(arg){
			return arg.binnen_os_selectie >=1;
		}
    	state.personeelsleden.forEach(function(person){
    		person.contracten.forEach(function(contract){
    			person.betrekkingen_total = person.betrekkingen_total.concat(contract.betrekkingen);// all betrekkingen from all contracts together
				person.betrekking_current = person.betrekkingen_total.filter(checkBretekking); // all betrekkingen from all contracts in current Neveau.
				contract.betrekkingen.forEach(function (betrekking, index) {
					state.betrekkingen_map[betrekking.data.id] = {personeelslid_id: betrekking.data.personeelslid_id, index: index, contract_id: betrekking.data.contract_id};
				});
			});
		});

		// state.personeelsleden[state.personeels_map[personeelslid_id]].contracten[state.contracten_map[contract_id]].betrekkingenLoaded = true;
		// state.personeelsleden[state.personeels_map[personeelslid_id]].contracten[state.contracten_map[contract_id]].betrekkingen.forEach(function(betrekking, index) {
		// 	state.betrekkingen_map[betrekking.data.id] = {personeelslid_id: personeelslid_id, index: index, contract_id: contract_id};
		// });
	},
	[mutations.TOGGLE_EDIT_BETREKKING]: (state, {betrekking_id, personeelslid_id, contract_id}) =>{
		let betrekking_mapped = state.betrekkingen_map[betrekking_id];
		let betrekkingEdit = {};
		state.personeelsleden[state.personeels_map[personeelslid_id]].contracten.forEach(function(contract){
			if(contract.data.id === contract_id){
				betrekkingEdit = contract.betrekkingen[betrekking_mapped.index].edit;
				return betrekkingEdit;
			}
		});
		// let betrekking_edit = state.personeelsleden[state.personeels_map[mapped.personeelslid_id]].contracten[state.contracten_map[mapped.personeelslid_id]].betrekkingen[mapped.index].edit;
		betrekkingEdit.active = !betrekkingEdit.active;
	},
    [mutations.DELETE_BETREKKING]:(state, betrekking_id) =>{
        let mapped = state.betrekkingen_map[betrekking_id];
        state.personeelsleden[state.personeels_map[mapped.personeelslid_id]].betrekkingen.splice(mapped.index, 1);
        delete state.betrekkingen_map[betrekking_id];
    },
	//__________________________STORE_MODELS________________________________________________________________________
	[mutations.STORE_CAOS]:(state, item) =>{
		if (!state.caos[item.cao_id]) {
			state.caos[item.cao_id] = {
				id: item.cao_id,
				code: item.cao_code,
				name: item.cao_name,
				schalen: {}
			};
		}
	},
	[mutations.STORE_SCHALEN]:(state, item) =>{
		if (!state.caos[item.cao_id].schalen[item.salarisschaal_id]) {
			state.caos[item.cao_id].schalen[item.salarisschaal_id] = {
				id: item.salarisschaal_id,
				code: item.salarisschaal_code,
				name: item.salarisschaal_name,
				treden: {}
			};
		}
	},
	[mutations.STORE_TREDEN]:(state, item) => {
		state.caos[item.cao_id].schalen[item.salarisschaal_id].treden[item.salaristrede_id] = {
			id: item.salaristrede_id,
			code: item.salaristrede_code,
			name: item.salaristrede_name
		};
	},
	[mutations.UPDATE_SELECTED_CAO_COSTS]:(state, costs) =>{
		state.selected_cao_costs = costs;
	},
	[mutations.STORE_KOSTENPLAATS]:(state, kostenplaats)=> {
		state.kostenplaatsen[kostenplaats.id] = kostenplaats.name;
	},
	[mutations.STORE_FUNCTIECATEGORIE]:(state, functiecategorie) =>{
		state.functiecategorien[functiecategorie.id] = functiecategorie.code;
	},
    [mutations.UPDATE_DISPLAY_VALUE_TYPE]: (state, {type, show}) =>{
        state.display_value_type[type] = show;
    },
    [mutations.PERSONEEL_TOTALEN_MAANDEN_LOADED]: (state, personeelslid_id) =>{
        state.personeelsleden[state.personeels_map[personeelslid_id]].totalenMaandenLoaded = true;
    },
    [mutations.RESET_EDIT_STATES]: (state, {except_personeelslid_id, except_betrekking_id}) =>{
        state.personeelsleden.forEach(function(personeelslid) {
            personeelslid.edit.error = false;
            if (except_personeelslid_id !== personeelslid.data.id) {
                personeelslid.edit.active = false;
                personeelslid.edit.highlighted = false;
            }
            personeelslid.contracten.forEach(function(contract){
				contract.betrekkingen.forEach(function(betrekking) {
					betrekking.edit.error = false;
					if (except_betrekking_id !== betrekking.data.id) {
						betrekking.edit.active = false;
						betrekking.edit.highlighted = false;
					}
				});
			});
        });
    },
    [mutations.RESET_DETAILVIEW_STATES]: (state, except_personeelslid_id) =>{
        state.personeelsleden.forEach(function(personeelslid) {
            if (except_personeelslid_id !== personeelslid.data.id) {
                personeelslid.detail.active = false;
            }
        });
    },
    [mutations.TOGGLE_VIEW_PERSONEELSLID]: (state, personeelslid_id) =>{
        let personeelslid_detail = state.personeelsleden[state.personeels_map[personeelslid_id]].detail;
        personeelslid_detail.active = !personeelslid_detail.active;
    },
    [mutations.TOGGLE_EDIT_PERSONEELSLID]: (state, personeelslid_id) =>{
        let personeelslid_edit = state.personeelsleden[state.personeels_map[personeelslid_id]].edit;
        personeelslid_edit.active = !personeelslid_edit.active;
    },
    [mutations.HIGHLIGHT_PERSONEELSLID]: (state, {personeelslid_id}) =>{
        state.personeelsleden[state.personeels_map[personeelslid_id]].edit.highlighted = true;
    },
    [mutations.HIGHLIGHT_BETREKKING_AND_PERSONEELSLID]:(state, betrekking_id) =>{
        let mapped = state.betrekkingen_map[betrekking_id.betrekking_id];
        state.personeelsleden[state.personeels_map[mapped.personeelslid_id]].edit.highlighted = true;
        state.personeelsleden[state.personeels_map[mapped.personeelslid_id]].betrekkingen[mapped.index].edit.highlighted = true;
    },
    [mutations.HIGHLIGHT_OTHER_BETREKKINGEN_FROM_CONTRACT]: (state, betrekking_id) =>{
        let mapped = state.betrekkingen_map[betrekking_id.betrekking_id];
        let betrekking = state.personeelsleden[state.personeels_map[mapped.personeelslid_id]].betrekkingen[mapped.index];
        let personeelslid = state.personeelsleden[state.personeels_map[betrekking.data.personeelslid_id]];
        personeelslid.betrekkingen.forEach(function(item) {
            if (item.data.contract_id === betrekking.data.contract_id && item.data.id !== betrekking.data.id) {
                item.edit.highlighted = true;
            }
        });
    },
    [mutations.PERSONEELSLID_EDIT_ERROR]: (state, personeelslid_id) =>{
        state.personeelsleden[state.personeels_map[personeelslid_id]].edit.error = true;
    },
    [mutations.PERSONEELSLID_BETREKKING_EDIT_ERROR]: (state, betrekking_id) =>{
        let mapped = state.betrekkingen_map[betrekking_id];
        state.personeelsleden[state.personeels_map[mapped.personeelslid_id]].edit.error = true;
        state.personeelsleden[state.personeels_map[mapped.personeelslid_id]].betrekkingen[mapped.index].edit.error = true;
    },
    [mutations.MASK_PERSONEELSLID]: (state, {personeelslid_id, mask_text}) =>{
        let personeelslid_edit = state.personeelsleden[state.personeels_map[personeelslid_id]].edit;
        personeelslid_edit.masked = true;
        personeelslid_edit.mask_text = mask_text;
    },
    [mutations.UNMASK_PERSONEELSLID]: (state, {personeelslid_id}) =>{
        let personeelslid_edit = state.personeelsleden[state.personeels_map[personeelslid_id]].edit;
        personeelslid_edit.masked = false;
        personeelslid_edit.mask_text = '';
    },
    [mutations.HIDE_MODAL]: (state) =>{
        state.show_modal = false;
        state.modalComponent = null;
        state.modalData = null;
    },
    [mutations.SHOW_MODAL_COMPONENT]: (state, {component, data}) =>{
        state.modalData = data;
        state.modalComponent = component;
        state.show_modal = true;
    },
    [mutations.UPDATE_MODALDATA]: (state, {key, value}) =>{
        state.modalData[key].value = value;
        state.modalData[key].isValid = true;
    },
    [mutations.INVALIDATE_INPUT]: (state, key) =>{
        state.modalData[key].isValid = false;
    },
    [mutations.UPDATE_ACTIVE_SORT]: (state, key) =>{
        state.activeSortIndex = key;
        // state.loader = true;
    },
    [mutations.UPDATE_ACTIVE_INTERVAL]: (state, payload) =>{
        state.activeIntervalVal = payload;
        // state.loader = true;
    },
	[mutations.LOADER_ON]:(state)=>{
    	state.loader = true;
	},
	[mutations.LOADER_OFF]:(state)=>{
    	state.loader = false;
	}
}