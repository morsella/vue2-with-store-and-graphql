import VueResource from 'vue-resource';

const PersoneelsTotalMandenQuery = VueResource("index.html?module=something&action=formatie&saction=personeeltotalenmaanden&personeelslid_id={personeelslid_id}");
export default {
    PersoneelsTotalMandenQuery
}