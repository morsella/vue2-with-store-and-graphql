import * as types from '../../types';
import mutations from './mutations';
import actions from './actions';

const state = {
  currentNiveau: {},
  currentNiveauChildren: {},
  ongekkopeldeFinancieele: {},
  ongekkopeldePersonele: {},
  logs: [],
  tree: {},
  tree_render_mode: 'normal',
  selected_indices: [0,0,0,0,0,0,0],
  selected_item:{
      'financieel_groeperingen': {},
      'personeel_groeperingen': {}
  },
    created_obj: {},
    last_obj_created:[],
    last_selected_item: {},
    origin_obj:{},
    errors: ' '
};

const getters = {
    [types.CURRENT_NIVEAU]: state =>{
        return state.currentNiveau;
    },
    [types.CURRENT_NIVEAU_CHILDREN]: state =>{
        return state.currentNiveauChildren;
    },
    [types.ONGEKKOPELDE_FINANCIEELE]: state =>{
        return state.ongekkopeldeFinancieele;
    },
    [types.ONGEKKOPELDE_PERSONELE]: state =>{
        return state.ongekkopeldePersonele;
    },
    [types.LOGS_LIST]:state =>{
        return state.logs;
    },
    [types.TREE]:state =>{
        return state.tree;
    },
    [types.TREE_RENDER_MODE]:state =>{
        return state.tree_render_mode;
    },
    [types.SELECTED_INDICES]:state =>{
        return state.selected_indices;
    },
    [types.SELECTED_ITEM]:state =>{
        return state.selected_item;
    },
    [types.CREATED_OBJ]:state =>{
        return state.created_obj;
    },
    [types.LAST_OBJ_CREATED]:state =>{
        return state.last_obj_created;
    },
    [types.LAST_SELECTED_ITEM]: state =>{
        return state.last_selected_item;
    },
    [types.ORIGIN_OBJ]: state =>{
        return state.origin_obj;
    },
    [types.ERRORS]: state =>{
        return state.errors;
    }

};

export default{
    state,
    getters,
    mutations,
    actions
}
