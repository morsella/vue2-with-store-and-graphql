//________BEGROTING_FORMATIE________________________________________________________

//Getters
export const PERSONEELS_MAP = 'formatie/PERSONEELS_MAP';
export const BETREKKINGEN_MAP = 'formatie/BETREKKINGEN_MAP';
export const PERSONEELSLEDEN = 'formatie/PERSONEELSLEDEN';
export const JAREN = 'formatie/JAREN';
export const LAAGSTE_JAAR = 'formatie/LAAGSTE_JAAR';
export const HOOGSTE_JAAR = 'formatie/HOOGSTE_JAAR';
export const CAOS = 'formatie/CAOS';
export const SELECTED_CAO_COSTS = 'formatie/SELECTED_CAO_COSTS';
export const KOSTENPLAATSEN = 'formatie/KOSTENPLAATSEN';
export const FUNCTIECATEGORIEN = 'formatie/FUNCTIECATEGORIEN';
export const DISPLAY_VALUE_TYPE = 'formatie/DISPLAY_VALUE_TYPE';
export const SHOW_MODAL = 'formatie/SHOW_MODAL';
export const MODAL_COMPONENT = 'formatie/MODAL_COMPONENT';
export const MODALDATA = 'formatie/MODALDATA';
export const ACTIVESORT = 'formatie/ACTIVESORT';
export const SORT_OPTIONS = 'formatie/SORT_OPTIONS';
export const CONTEXT = 'formatie/CONTEXT';
export const INTERVALS = 'formatie/INTERVALS';
export const ACTIVE_INTERVAL = 'formatie/ACTIVE_INTERVAL';
export const ALL_DATES = 'formatie/ALL_DATES';
export const PERIODES_ARRAY = 'formatie/PERIODES_ARRAY';
export const CONTRACTEN_MAP = 'formatie/CONTRACTEN_MAP';
export const AMOUNTS_PERSONEELSLEDEN = 'formatie/AMOUNTS_PERSONEELSLEDEN';
export const LOADER = 'formatie/LOADER';





