import Vue from 'vue';
import Vuex from 'vuex';
import formatie from './modules/formatie/formatie';
import VuewResource from 'vue-resource';

Vue.use(Vuex);
Vue.use(VuewResource);

export default new Vuex.Store({
    modules:{
        formatie
    }
});
