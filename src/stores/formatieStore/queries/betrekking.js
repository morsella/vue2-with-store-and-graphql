import VueResource from 'vue-resource';

const BetrekkingQuery = VueResource('index.html?module=something&action=formatie&saction=betrekkingen&id={id}');
export default {
    BetrekkingQuery
}