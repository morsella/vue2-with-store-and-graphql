import * as types from '../../types';
import GraphQLNiveaus from '../../queries/groeperingen.graphql';
import Ongekkopeld from '../../queries/ongekkopeld.graphql';
import HuidigOsNiveau from '../../queries/huidig_niveau.graphql';
import MutationFinancieelGraphQL from '../../queries/mutation_financieel.graphql';
import MutationPersoneelGraphQL from '../../queries/mutation_personeel.graphql';
import Vue from 'vue';

export default {
    [types.ACTION_CURRENT_NIVEAU]:({commit, state})=>{
        fetch('?graphql', {
            credentials: 'same-origin',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                query :HuidigOsNiveau
            })
        }).then(response => {
            return response.json();
        }).then(data => {
            let tmp = data.data.huidig_os_niveau;
            let payload = {id: tmp.id, naam: tmp.naam, code: tmp.code,
                financieel_groeperingen: tmp.financieel_groeperingen,
                personeel_groeperingen: tmp.personeel_groeperingen,
                accepteert_personeel_groeperingen: tmp.accepteert_personeel_groeperingen,
                accepteert_financieel_groeperingen: tmp.accepteert_financieel_groeperingen
            };
            commit(types.MUTATE_CURRENT_NIVEAU, payload);
            this.a['tree/ACTION_RENDER_TREE']({commit, state});
            this.a['tree/ACTION_ONGEKKOPELDE']({commit, state});

        }).catch(ex=>{
        });
    },
    [types.ACTION_RENDER_TREE]:({commit, state})=>{
        let id = state.currentNiveau.id;
        fetch('?graphql', {
            credentials: 'same-origin',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                query :GraphQLNiveaus,
                variables: {
                    "os_niveau_id": id
                }
            })
        }).then(response => {
            return response.text().then(function(text) {
                return JSON.parse(text);
            });
        }).then(data => {
            let tmp =  data.data.os_niveaus[0];
            let payload = {id: tmp.id, naam: tmp.naam, code: tmp.code,
                financieel_groeperingen: tmp.financieel_groeperingen,
                personeel_groeperingen: tmp.personeel_groeperingen,
                accepteert_personeel_groeperingen: tmp.accepteert_personeel_groeperingen,
                accepteert_financieel_groeperingen: tmp.accepteert_financieel_groeperingen,
                children: tmp.children
            };
            commit(types.MUTATE_RENDER_TREE, payload);
        }).catch(ex=>{
        });

    },
    [types.ACTION_TREE_RENDER_MODE]:({commit}, render_mode)=>{
        commit(types.MUTATE_TREE_RENDER_MODE, render_mode);
    },
    [types.ACTION_ONGEKKOPELDE]:({commit, state}) =>{
        let id = state.currentNiveau.id;
        fetch('?graphql',{
            credentials: 'same-origin',
            method: 'POST',
            headers:{
                'Content-Type':'application/json'
            },
            body: JSON.stringify({
                query: Ongekkopeld,
                variables:{
                    "os_niveau_id": id
                }
            })
        }).then(response =>{
            return response.json();
        }).then(data =>{
            let tmp = data.data.ongekoppelde_groeperingen;
            let payload = {
                ongekoppelde_financiele_groeperingen: tmp.financieel_groeperingen,
                ongekoppelde_personele_groeperingen: tmp.personeel_groeperingen
            };
            commit(types.MUTATE_ONGEKKOPELDE,payload);
        }).catch(ex =>{
        });
    },
    [types.ACTION_UPDATE_LIST]:({commit, state}, payload) =>{
        commit(types.MUTATE_UPDATE_LIST,payload);
        this.a['tree/ACTION_UPDATE_DB']({commit, state});
    },
    [types.ACTION_UPDATE_LIST_LEVELS]:({commit, state}, payload) =>{
        if (payload.origin_id){
            let query = (payload.type === 'financieel_groeperingen' ? MutationFinancieelGraphQL : MutationPersoneelGraphQL );
            let variables = {
                "action": 'unlink',
                "osn": {
                    "id": payload.origin_id
                },
                "gId": {
                    "id": payload.id
                }
            };
            Vue.http.post('./index.html?graphql', {query: query, variables: variables })
                .then(response =>{
                    if (response.body.errors) {
                        commit(types.MUTATE_ERRORS, response.body.errors);
                    } else {
                        commit(types.MUTATE_ERRORS, ' unlink success');
                        commit(types.MUTATE_UPDATE_LIST_LEVELS, payload);
                        this.a['tree/ACTION_UPDATE_DB']({commit, state});
                    }
                },error =>{
                    commit(types.MUTATE_ERRORS, error);
                });
        } else {
            commit(types.MUTATE_UPDATE_LIST_LEVELS, payload);
            this.a['tree/ACTION_UPDATE_DB']({commit, state});
        }
    },
    [types.ACTION_NEXT_LEVEL]:({commit}, payload) =>{
        commit(types.MUTATE_NEXT_LEVEL, payload);
    },
    [types.ACTION_SELECTED_ITEM_CHANGED]: ({commit}, payload) =>{
        commit(types.MUTATE_SELECTED_ITEM_CHANGED, payload);
    },
    [types.ACTION_UPDATE_DB]:({commit, state}) =>{
        let obj = state.created_obj;
        let query = (obj.type === 'financieel_groeperingen' ? MutationFinancieelGraphQL : MutationPersoneelGraphQL );
        let variables = {
            "action": obj.action,
            "osn": {
                "id": obj.targetId
            },
            "gId": {
                "id": obj.groeperingId
            }
        };
        Vue.http.post('./index.html?graphql', {query: query, variables: variables })
            .then(response =>{
                if(response.body.errors){
                    if(response.body.errors[0].message === 'Illegal action "" selected'){
                        commit(types.MUTATE_ERRORS,' ');
                    }else{
                        commit(types.MUTATE_ERRORS, response.body.errors);
                    }
                }else{
                    commit(types.MUTATE_ERRORS, 'succesvol opgeslagen');
                }
            },error =>{
                commit(types.MUTATE_ERRORS, error);
        });
    },
    [types.ACTION_UNDO_LAST_LINKING]: ({commit, state},payload) =>{
        if (payload.dropped_obj.origin_id){
            let query = (payload.dropped_obj.type === 'financieel_groeperingen' ? MutationFinancieelGraphQL : MutationPersoneelGraphQL );
            let variables = {
                "action": 'unlink',
                "osn": {
                    "id": payload.dropped_obj.destination.id
                },
                "gId": {
                    "id": payload.dropped_obj.id
                }
            };
            Vue.http.post('./index.html?graphql', {query: query, variables: variables })
                .then(response =>{
                    if (response.body.errors) {
                        commit(types.MUTATE_ERRORS, response.body.errors);
                    } else {
                        commit(types.MUTATE_ERRORS, 'unlink success undo');
                        commit(types.MUTATE_UNDO_LAST_LINKING, payload);
                        this.a['tree/ACTION_UPDATE_DB']({commit, state});
                    }
                },error =>{
                    commit(types.MUTATE_ERRORS, error);
                });
        } else {
            commit(types.MUTATE_UNDO_LAST_LINKING, payload);
            this.a['tree/ACTION_UPDATE_DB']({commit, state});
        }
    },
    [types.ACTION_ORIGIN_OBJ]:({commit}, payload)=>{
        commit(types.MUTATE_ORIGIN_OBJ, payload);
    }
}
