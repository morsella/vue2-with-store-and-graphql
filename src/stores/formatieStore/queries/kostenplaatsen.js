import VueResource from 'vue-resource';

const KostenplaatsenQuery = VueResource("index.html?module=something&action=formatie&saction=kostenplaatsen");
export default {
    KostenplaatsenQuery
}