import * as actions from '../../types/actions';
import * as mutations from '../../types/mutations';
import * as resources from '../../types/resources';
import Vue from 'vue';
// import TredeAmountQuery from '../../queries/trede_amount_for_period';
// import JarenQuery from '../../queries/jaren';
// import CaosQuery from '../../queries/caos';
// import KostenplaatsenQuery from '../../queries/kostenplaatsen';
// import PersoneelQuery from '../../queries/personeels';
// import BetrekkingQuery from '../../queries/betrekking';
// import FunctiecategorienQuery from '../../queries/functiecategorien';
// import PersoneelTotalMandenQuery from '../../queries/personeel_total_maanden';
import Context from '../../queries/context.graphql';
import Personeelsleden from '../../queries/personeelsleden.graphql';
import Betrekkingen from '../../queries/betrekkingen.graphql';


export default {
    [actions.SHOW_MODAL]:({commit}, {component, data}) =>{
        commit(mutations.RESET_EDIT_STATES, {except_personeelslid_id: null, except_betrekking_id: null});
        commit(mutations.SHOW_MODAL_COMPONENT, {component: component, data: data});
    },
    [actions.SHOW_MODAL_PERSONEELSLID]:({commit}, {component, id, data}) =>{
        this.a['formatie/SHOW_MODAL']({commit}, {component: component, data: data});
        commit(mutations.HIGHLIGHT_PERSONEELSLID, {personeelslid_id: id} );
    },
    [actions.SHOW_MODAL_BETREKKING]:({commit}, {component, id, data})=>{
        this.a['formatie/SHOW_MODAL']({commit}, {component: component, data: data});
        commit(mutations.HIGHLIGHT_BETREKKING_AND_PERSONEELSLID, {betrekking_id: id});
    },
    [actions.HIDE_MODAL]:({commit}) =>{
        commit(mutations.RESET_EDIT_STATES, {except_personeelslid_id: null, except_betrekking_id: null});
        commit(mutations.HIDE_MODAL);
    },
    [actions.HIGHLIGHT_OTHER_BETREKKINGEN_FROM_CONTRACT]:({commit}, betrekking_id) =>{
        commit(mutations.HIGHLIGHT_OTHER_BETREKKINGEN_FROM_CONTRACT, betrekking_id);
    },
    [actions.UPDATE_MODAL_DATA]:({commit}, {key, value}) =>{
        commit(mutations.UPDATE_MODALDATA, {key: key, value: value});
    },
    [actions.UPDATE_MODAL_DATA_NUMBER]:({commit}, {key, value}) =>{
        let number = value.replace(',', '.');
        let self = this;
        if (number === '') {
            self.a['formatie/UPDATE_MODAL_DATA']({commit}, {key: key, value: number});
        } else if (!isNumber(number)) {
            commit(mutations.INVALIDATE_INPUT, key);
        } else {
            self.a['formatie/UPDATE_MODAL_DATA']({commit}, {key: key, value: parseFloat(number)});
        }
    },
    [actions.MASK_PERSONEELSLID]:({commit}, {personeelslid_id, mask_text}) =>{
        commit(mutations.MASK_PERSONEELSLID, {personeelslid_id: personeelslid_id, mask_text: mask_text});
    },
    [actions.UNMASK_PERSONEELSLID]:({commit}, personeelslid_id) =>{
        commit(mutations.UNMASK_PERSONEELSLID, personeelslid_id);
    },
    [actions.UPDATE_SELECTED_CAO_COSTS]: ({commit, state}) =>{
        if (state.modalData.trede.value && state.modalData.jaar.value && state.modalData.maand.value) {
            let getObject = {id: state.modalData.trede.value, date: state.modalData.jaar.value + '-' + state.modalData.maand.value + '-01'};
            // To fetch the Trade amount
            Vue.resource('index.html?module=something&action=formatie&saction=tredebedrag&salaristrede_id={id}&datum={date}').get(getObject).then(response => {
                return response.ok ? response.json() : 0;
            }).then(data => {
                commit(mutations.UPDATE_SELECTED_CAO_COSTS, data);
            });
        } else {
            commit(mutations.UPDATE_SELECTED_CAO_COSTS, 0);
        }
    },
    [actions.SHOW_MODAL_ADD_VACATURE]:({commit, state}) =>{
        let self = this;
        self.a['formatie/SHOW_MODAL']({commit}, {component: 'modal-add-vacature', data: {
                id: null,
                description: {
                    value: ''
                },
                startdate: {
                    value: state.laagste_jaar + '-01-01'
                },
                enddate: {
                    value: state.hoogste_jaar + '-12-01'
                },
                functiecategorie: {
                    value: ''
                },
                functiondescription: {
                    value: ''
                },
                wtf: {
                    value: ''
                },
                toelage: {
                    value: ''
                },
                kostenplaats: {
                    value: ''
                },
                cao: {
                    value: ''
                },
                schaal: {
                    value: ''
                },
                trede: {
                    value: ''
                }
            }
        });
    },
    [actions.SAVE_MODAL_AND_VACATURE]:({commit, state}) =>{
        let self = this;
        self.a['formatie/HIDE_MODAL']({commit});
        // this.$http.post(
        // 	'something', // @TODO fix this post...
        // 	{
        // 		data: state.modalData // But more explicit
        // 	}
        // ).then(
        // 	function(response) { // success
        // 		// Dispatch adding the vacature to the store...
        // 		toastr.info('Vacature succesvol toegevoegd');
        // 	},
        // 	function(response) { // error
        // 		toastr.error(response); // check and fix this
        // 	}
        // );
    },
    [actions.SHOW_MODAL_WTF]:({commit, state}, data) =>{
        let personeelslid = state.personeelsleden[state.personeels_map[data.data.personeelslid_id]];
        let self = this;
        self.a['SHOW_MODAL_BETREKKING']({commit}, {component: 'modal-wtf', id: data.data.id, data:{
            id: data.data.id,
            begindatum: data.data.begindatum,
            einddatum: data.data.einddatum,
            personeelslid: {
                id: personeelslid.data.id,
                naam: personeelslid.data.naam,
                vacature: personeelslid.data.vacature,
                betrekkingen: personeelslid.betrekkingen
            },
            wtf: {
                value: data.data.wtf,
                isValid: true
            },
            startdate: {
                value: data.data.begindatum,
                isValid: true
            },
            enddate: {
                value: data.data.einddatum,
                isValid: true
            }
        }});
    },
    [actions.SAVE_MODAL_WTF]:({commit, state}) =>{
        let betrekking_id = state.modalData.id;
        let personeelslid_id = state.modalData.personeelslid.id;
        let self = this;
        self.a['MASK_PERSONEELSLID']({commit}, {personeelslid_id:personeelslid_id, mask_text:'WTF Wijziging doorvoeren...'  });
        // this.$http.post('something/', { // @TODO fix this post...
        // 	data: store.state.modalData // But more explicit
        // }).then(
        // 	function(response) { // success
        // 		// Dispatch adding the data to the store...
        // 		toastr.info('WTF wijziging(en) succesvol doorgevoerd');
        // 		unmaskPersoneelslid(store, personeelslid_id);
        // 	},
        // 	function(response) { // error
        // 		store.dispatch('PERSONEELSLID_BETREKKING_EDIT_ERROR', betrekking_id);
        // 		toastr.error(response); // check and fix this
        // 		unmaskPersoneelslid(store, personeelslid_id);
        // 	}
        // );
        setTimeout(function(){
            self.a['UNMASK_PERSONEELSLID']({commit}, {personeelslid_id: personeelslid_id});
            }, 2000);
            self.a['formatie/HIDE_MODAL']({commit});
    },
    [actions.SHOW_MODAL_DATES]:({commit, state}, data) =>{
        let personeelslid = state.personeelsleden[state.personeels_map[data.data.personeelslid_id]];
        let self = this;
        self.a['formatie/SHOW_MODAL_BETREKKING']({commit}, {component: 'modal-dates', id: data.data.id, data: {
                id: data.data.id,
                begindatum: data.data.begindatum,
                einddatum: data.data.einddatum,
                personeelslid: {
                    id: personeelslid.data.id,
                    naam: personeelslid.data.naam,
                    vacature: personeelslid.data.vacature
                },
                startdate: {
                    value: data.data.begindatum
                },
                enddate: {
                    value: data.data.einddatum
                }
            }
        });
    },
    [actions.SAVE_MODAL_DATES]:({commit, state}) =>{
        let betrekking_id = state.modalData.id;
        let personeelslid_id = state.modalData.personeelslid.id;
        let self = this;
        self.a['formatie/MASK_PERSONEELSLID']({commit}, {personeelslid_id: personeelslid_id, mask_text: 'Datum Wijziging doorvoeren...' });
        // this.$http.post('sometging/', { // @TODO fix this post...
        // 	data: store.state.modalData // But more explicit
        // }).then(
        // 	function(response) { // success
        // 		// Dispatch adding the data to the store...
        // 		toastr.info('Datum wijziging(en) succesvol doorgevoerd');
        // 		unmaskPersoneelslid(store, personeelslid_id);
        // 	},
        // 	function(response) { // error
        // 		store.dispatch('PERSONEELSLID_BETREKKING_EDIT_ERROR', betrekking_id);
        // 		toastr.error(response); // check and fix this
        // 		unmaskPersoneelslid(store, personeelslid_id);
        // 	}
        // );
        setTimeout(function(){
            self.a['formatie/UNMASK_PERSONEELSLID']({commit}, {personeelslid_id: personeelslid_id});
            }, 2000);
            self.a['formatie/HIDE_MODAL']({commit});
    },
    [actions.SHOW_MODAL_SCHAAL]:({commit, state},data) =>{
        let personeelslid = state.personeelsleden[state.personeels_map[data.data.personeelslid_id]];
        let begindatum = data.data.contract_begindatum.split('-');
        let laagste_maand = '01';
        let laagste_jaar = state.laagste_jaar;

        if (parseInt(begindatum[0]) > parseInt(laagste_jaar)) {
            laagste_jaar = begindatum[0];
            laagste_maand = begindatum[1];
        }
        this.a['formatie/SHOW_MODAL_BETREKKING']({commit}, {component: 'modal-schaal' , id: data.data.id, data: {
            id: data.data.id,
            begindatum: data.data.begindatum,
            einddatum: data.data.einddatum,
            personeelslid: {
                id: personeelslid.data.id,
                naam: personeelslid.data.naam,
                vacature: personeelslid.data.vacature
            },
            contract: {
                begindatum: data.data.contract_begindatum,
                einddatum: data.data.contract_einddatum
            },
            cao: {
                original_value: data.data.cao_id,
                value: data.data.cao_id
            },
            schaal: {
                original_value: data.data.salarisschaal_id,
                value: data.data.salarisschaal_id
            },
            trede: {
                original_value: data.data.laagste_salaristrede_id,
                value: data.data.laagste_salaristrede_id
            },
            maand: {
                original_value: laagste_maand,
                value: laagste_maand
            },
            jaar: {
                original_value: laagste_jaar,
                value: laagste_jaar
            }
        }});
        this.a['formatie/HIGHLIGHT_OTHER_BETREKKINGEN_FROM_CONTRACT']({commit}, {betrekking_id: data.data.id});
        this.a['formatie/UPDATE_SELECTED_CAO_COSTS']({commit, state});
    },
    [actions.SAVE_MODAL_SCHAAL]:({commit, state})=>{
        let betrekking_id = state.modalData.id;
        let personeelslid_id = state.modalData.personeelslid.id;
        let self = this;
        self.a['formatie/MASK_PERSONEELSLID']({commit}, {personeelslid_id: personeelslid_id, mask_text:'Schaal wijziging doorvoeren...'});
        // this.$http.post('something', { // @TODO fix this post...
        // 	data: store.state.modalData // But more explicit
        // }).then(
        // 	function(response) { // success
        // 		// Dispatch adding the data to the store...
        // 		toastr.info('Schaal wijziging(en) succesvol doorgevoerd');
        // 		unmaskPersoneelslid(store, personeelslid_id);
        // 	},
        // 	function(response) { // error
        // 		store.dispatch('PERSONEELSLID_BETREKKING_EDIT_ERROR', betrekking_id);
        // 		toastr.error(response); // check and fix this
        // 		unmaskPersoneelslid(store, personeelslid_id);
        // 	}
        // );
        setTimeout(function(){
            self.a['formatie/UNMASK_PERSONEELSLID']({commit}, {personeelslid_id: personeelslid_id});
        }, 2000);
        self.a['formatie/HIDE_MODAL']({commit});
    },
    [actions.SHOW_MODAL_TOELAGES]:({commit, state}, data)=>{
        let personeelslid = state.personeelsleden[state.personeels_map[data.data.personeelslid_id]];
        this.a['formatie/SHOW_MODAL_BETREKKING']({commit}, {component: 'modal-toelages', id: data.data.id, data: {
            id: data.data.id,
            begindatum: data.data.begindatum,
            einddatum: data.data.einddatum,
            personeelslid: {
                id: personeelslid.data.id,
                naam: personeelslid.data.naam,
                vacature: personeelslid.data.vacature,
                betrekkingen: personeelslid.betrekkingen
            },
            toelage: {
                original_value: data.data.toelage,
                value: data.data.toelage,
                isValid: true
            },
            toelageBhv: {
                original_value: data.data.toelage_bhv,
                value: data.data.toelage_bhv,
                isValid: true
            },
            toelageFitness: {
                original_value: data.data.toelage_fitness,
                value: data.data.toelage_fitness,
                isValid: true
            },
            toelageReiskosten: {
                original_value: data.data.toelage_reiskosten,
                value: data.data.toelage_reiskosten,
                isValid: true
            },
            inkomenstoelage: {
                original_value: data.data.inkomenstoelage,
                value: data.data.inkomenstoelage,
                isValid: true
            }
        } });
    },
    [actions.SAVE_MODAL_TOELAGES]:({commit, state}) =>{
        let betrekking_id = state.modalData.id;
        let personeelslid_id = state.modalData.personeelslid.id;
        let self = this;
        self.a['formatie/MASK_PERSONEELSLID']({commit}, {personeelslid_id: personeelslid_id, mask_text:'Toelage Wijziging doorvoeren...' });
        // this.$http.post('something', { // @TODO fix this post...
        // 	data: store.state.modalData // But more explicit
        // }).then(
        // 	function(response) { // success
        // 		// Dispatch adding the data to the store...
        // 		toastr.info('Toelage wijziging(en) succesvol doorgevoerd');
        // 		unmaskPersoneelslid(store, personeelslid_id);
        // 	},
        // 	function(response) { // error
        // 		store.dispatch('PERSONEELSLID_BETREKKING_EDIT_ERROR', betrekking_id);
        // 		toastr.error(response); // check and fix this
        // 		unmaskPersoneelslid(store, personeelslid_id);
        // 	}
        // );
        setTimeout(function(){
            self.a['formatie/UNMASK_PERSONEELSLID']({commit}, {personeelslid_id: personeelslid_id});
            }, 2000);
            self.a['formatie/HIDE_MODAL']({commit});
    },
    [actions.SHOW_MODAL_DELETE_BETREKKING]:({commit, state},data) =>{
        let personeelslid = state.personeelsleden[state.personeels_map[data.data.personeelslid_id]];
        this.a['formatie/SHOW_MODAL_BETREKKING']({commit}, {component:'modal-delete-betrekking', id: data.data.id, data:{
            id: data.data.id,
            begindatum: data.data.begindatum,
            einddatum: data.data.einddatum,
            personeelslid: {
                id: personeelslid.data.id,
                naam: personeelslid.data.naam,
                vacature: personeelslid.data.vacature
            }
        } });
    },
    [actions.SAVE_MODAL_DELETE_BETREKKING]:({commit,state}) =>{
        let betrekking_id = state.modalData.id;
        let personeelslid_id = state.modalData.personeelslid.id;
        let self = this;
        self.a['formatie/MASK_PERSONEELSLID']({commit}, {personeelslid_id: personeelslid_id, mask_text: 'Betrekking verwijderen...'});
        // this.$http.post('something', { // @TODO fix this post...
        // 	data: store.state.modalData // But more explicit
        // }).then(
        // 	function(response) { // success
        // 		// Dispatch adding the data to the store...
        // 		toastr.info('Betrekking succesvol verwijderd');
        // 		unmaskPersoneelslid(store, personeelslid_id);
        // 	},
        // 	function(response) { // error
        // 		store.dispatch('PERSONEELSLID_BETREKKING_EDIT_ERROR', betrekking_id);
        // 		toastr.error(response); // check and fix this
        // 		unmaskPersoneelslid(store, personeelslid_id);
        // 	}
        // );
        setTimeout(function(){
            commit(mutations.DELETE_BETREKKING, betrekking_id);
            // store.dispatch('UPDATE_PERSONEELSLID', betrekking_id);
            // possibly delete personeelslid, add personeelslid and re-sort?
            self.a['formatie/UNMASK_PERSONEELSLID']({commit}, {personeelslid_id: personeelslid_id});
        }, 2000);
        self.a['formatie/HIDE_MODAL']({commit});
    },
    [actions.SHOW_MODAL_EDIT_PROPERTIES]:({commit, state}, data) =>{
        let personeelslid = state.personeelsleden[state.personeels_map[data.data.id]];
        this.a['formatie/SHOW_MODAL_PERSONEELSLID']({commit}, {component: 'modal-edit-properties', id: data.data.id, data:{
            id: personeelslid.data.id,
            naam: personeelslid.data.naam,
            vacature: personeelslid.data.vacature,
            personeelsnummer: personeelslid.data.personeelsnummer,
            geboortedatum: personeelslid.data.geboortedatum,
            geslacht: personeelslid.data.geslacht.substr(0, 1).toLowerCase() === 'm' ? 1 : 2,
            boventallig: 1 // @TODO add this property
        }});
    },
    [actions.SAVE_MODAL_EDIT_PROPRETIES]:({commit, state}) =>{
        let personeelslid_id = state.modalData.id;
        let self = this;
        self.a['formatie/MASK_PERSONEELSLID']({commit}, {personeelslid_id: personeelslid_id, mask_text: 'Eigenschappen wijzigen...' });
        // this.$http.post('something', { // @TODO fix this post...
        // 	data: store.state.modalData // But more explicit
        // }).then(
        // 	function(response) { // success
        // 		// Dispatch adding the data to the store...
        // 		toastr.info('Eigenschap wijziging(en) succesvol doorgevoerd');
        // 		unmaskPersoneelslid(store, personeelslid_id);
        // 	},
        // 	function(response) { // error
        // 		store.dispatch('PERSONEELSLID_EDIT_ERROR', personeelslid_id);
        // 		toastr.error(response); // check and fix this
        // 		unmaskPersoneelslid(store, personeelslid_id);
        // 	}
        // );
        setTimeout(function(){
            self.a['formatie/UNMASK_PERSONEELSLID']({commit}, {personeelslid_id: personeelslid_id});
            }, 2000);
            self.a['formatie/HIDE_MODAL']({commit});
    },
    [actions.SHOW_MODAL_EDIT_BETREKKING]:({commit, state}, data) =>{
        let personeelslid = state.personeelsleden[state.personeels_map[data.data.id]];
        this.a['formatie/SHOW_MODAL_PERSONEELSLID']({commit}, {component: 'modal-edit-betrekkingen', id: data.data.id, data:{
            id: personeelslid.data.id,
            naam: personeelslid.data.naam,
            vacature: personeelslid.data.vacature
        }});
    },
    [actions.SAVE_MODAL_EDIT_BETREKKING]:({commit, state}) =>{
        let personeelslid_id = state.modalData.id;
        let self = this;
        self.a['formatie/MASK_PERSONEELSLID']({commit}, {personeelslid_id: personeelslid_id, mask_text: 'Betrekkingen wijzigen...'});

        // this.$http.post('something', { // @TODO fix this post...
        // 	data: store.state.modalData // But more explicit
        // }).then(
        // 	function(response) { // success
        // 		// Dispatch adding the data to the store...
        // 		toastr.info('Betrekkingen wijziging(en) succesvol doorgevoerd');
        // 		unmaskPersoneelslid(store, personeelslid_id);
        // 	},
        // 	function(response) { // error
        // 		store.dispatch('PERSONEELSLID_EDIT_ERROR', personeelslid_id);
        // 		toastr.error(response); // check and fix this
        // 		unmaskPersoneelslid(store, personeelslid_id);
        // 	}
        // );
        setTimeout(function(){
            self.a['formatie/UNMASK_PERSONEELSLID']({commit}, {personeelslid_id: personeelslid_id});
            }, 2000);
            self.a['formatie/HIDE_MODAL']({commit});
    },
    [actions.SHOW_MODAL_ADD_BETREKKING]:({commit, state}, data) =>{
        let personeelslid = state.personeelsleden[state.personeels_map[data.data.id]];
        // Same modal as add Vacature?
        this.a['formatie/SHOW_MODAL_PERSONEELSLID']({commit}, {component: 'modal-add-betrekking', id: data.data.id, data:{
            id: personeelslid.data.id,
            naam: personeelslid.data.naam,
            vacature: personeelslid.data.vacature
        }});
    },
    [actions.SAVE_MODAL_ADD_BETREKKING]:({commit, state}) =>{
        let personeelslid_id = state.modalData.id;
        let self = this;
        self.a['formatie/MASK_PERSONEELSLID']({commit}, {personeelslid_id: personeelslid_id, mask_text: 'Betrekking toevoegen...'});

        // this.$http.post('something', { // @TODO fix this post...
        // 	data: store.state.modalData // But more explicit
        // }).then(
        // 	function(response) { // success
        // 		// Dispatch adding the data to the store...
        // 		toastr.info('Betrekking succesvol toegevoegd');
        // 		unmaskPersoneelslid(store, personeelslid_id);
        // 	},
        // 	function(response) { // error
        // 		store.dispatch('PERSONEELSLID_EDIT_ERROR', personeelslid_id);
        // 		toastr.error(response); // check and fix this
        // 		unmaskPersoneelslid(store, personeelslid_id);
        // 	}
        // );
        setTimeout(function(){ self.a['formatie/UNMASK_PERSONEELSLID']({commit}, {personeelslid_id: personeelslid_id});
        }, 2000);
        self.a['formatie/HIDE_MODAL']({commit});
    },
    [actions.SHOW_MODAL_DELETE_VACATURE]:({commit, state}, data) =>{
        let personeelslid = state.personeelsleden[state.personeels_map[data.data.id]];
        this.a['formatie/SHOW_MODAL_VACATURE']({commit}, {component: 'modal-delete-vacature', id: data.data.id, data:{
            id: personeelslid.data.id,
            naam: personeelslid.data.naam,
            vacature: personeelslid.data.vacature
        }});
    },
    [actions.SAVE_MODAL_DELETE_VACATURE]:({commit, state}) =>{
        let personeelslid_id = state.modalData.id;
        let self = this;
        self.a['formatie/MASK_PERSONEELSLID']({commit}, {personeelslid_id: personeelslid_id, mask_text: 'Vacature verwijderen...'});
        // this.$http.post('something', { // @TODO fix this post...
        // 	data: store.state.modalData // But more explicit
        // }).then(
        // 	function(response) { // success
        // 		// Dispatch adding the data to the store...
        // 		toastr.info('Vacature verwijderen mislukt');
        // 		unmaskPersoneelslid(store, personeelslid_id);
        // 	},
        // 	function(response) { // error
        // 		store.dispatch('PERSONEELSLID_EDIT_ERROR', personeelslid_id);
        // 		toastr.error(response); // check and fix this
        // 		unmaskPersoneelslid(store, personeelslid_id);
        // 	}
        // );
        setTimeout(function(){
           self.a['formatie/UNMASK_PERSONEELSLID']({commit}, {personeelslid_id: personeelslid_id});
            }, 2000);
            self.a['formatie/HIDE_MODAL']({commit});
    },
    [actions.SET_INTERVAL]:({commit}, event) =>{
		commit(mutations.LOADER_ON);
		commit(mutations.UPDATE_ACTIVE_INTERVAL, event.target.value);
    },
    [actions.UPDATE_JAREN_AND_PERIODES]:({commit, state}, payload) =>{
		commit(mutations.UPDATE_JAREN_AND_PERIODES, payload);
		console.log('commit periodes');
		if(state.periodes_array.length >= 1){
			this.a["formatie/LOAD_JAREN_AND_PERSONEELSLEDEN"]({commit, state});
        }
	},
    [actions.STORE_AMOUNTS]:({commit}, payload) =>{
    	commit(mutations.AMOUNTS_STORED, payload);
	},
    //__________________GRAPHQL RESOURCES_____________________________________________________________________________
    //________________________________________________________________________________________________________________

    [resources.LOAD_CONTEXT]:({commit}) =>{
        fetch('?graphql', {
            credentials: 'same-origin',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body:JSON.stringify({
                query: Context
            })
        }).then(response => {
				return response.text().then(function(text) {
					return JSON.parse(text);
				});
        }).then(data => {
            let payload = {os_niveau_id: data.data.financieel_context.os_niveau_id,
                            soort_id: data.data.financieel_context.soort_id,
                            begin: data.data.financieel_context.jaar_periode_van,
                            eind: data.data.financieel_context.jaar_periode_tot };
			commit(mutations.UPDATE_CONTEXT, payload);
			commit(mutations.STORE_UITERSTE_JAAREN);
        }).catch(ex => {
            console.log('error');
            console.log(ex);
        });
    },
    [resources.LOAD_JAREN_AND_PERSONEELSLEDEN]:({commit, state}) => {
        console.log('load personeel');
    	let context = state.context;
		let os_niveau_id = context.os_niveau_id;
		let soort_id = context.soort_id;
		let jaar_periode_van = context.begin;
		let jaar_periiode_tot = context.eind;
		let periodes = state.periodes_array;
		let total_range = [];
		total_range.push({min: state.allDates.start, max:state.allDates.end});
		if(state.context.os_niveau_id) {
			fetch('?graphql', {
				credentials: 'same-origin',
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					query: Personeelsleden,
					variables: {
						"financieel_context": {
							"os_niveau_id": os_niveau_id,
							"soort_id": soort_id,
							"jaar_periode_van": jaar_periode_van,
							"jaar_periode_tot": jaar_periiode_tot
						},
						"periodes": periodes,
						"total_range": total_range
					}
				})
			}).then(response => {
					return response.text().then(function(text) {
						return JSON.parse(text);
					});
			}, (response) => {
				console.log(`something went wrong when getting api/data the response says: ${response}`);
			}).then(data => {
			    commit(mutations.LOADER_ON);
			    //________________LOAD PERSONEELSLEDEN______________________________________________________________
				commit(mutations.TRUNCATE_PERSONEELSLID);
				data.data.formatie.personeelsleden.forEach(function (item) {
					commit(mutations.STORE_PERSONEELSLID, item);
				});
				commit(mutations.UPDATE_PERSONEELS_MAP);
				this.a["formatie/LOAD_BETREKKINGEN"]({commit, state});
			}).catch(ex => {
			    console.log(ex);
			});
		}
        // Loading jaren, then personeel and then setting floatThead has to be done synchronously!
        // Vue.resource('index.html?module=something&action=formatie&saction=jaren').get().then(function(response){
        //     response.data.forEach(function(item) { commit(mutations.STORE_JAAR, item); });
        //     commit(mutations.STORE_UITERSTE_JAAREN);
            // commit(mutations.STORE_PERSONEELSLEDEN);
            // commit(mutations.STORE_BETREKKINGEN);


            // setTimeout(function(){
            //     $("#demo").find("table.begroting_formatie").floatThead();
            // }, 1);
        // });
    },
    [resources.LOAD_BETREKKINGEN]:({commit, state}) =>{
		console.log('load betrekkingen');
		let context = state.context;
		let os_niveau_id = context.os_niveau_id;
		let soort_id = context.soort_id;
		let jaar_periode_van = context.begin;
		let jaar_periiode_tot = context.eind;
		let periodes = state.periodes_array;
		let total_range = [];
		total_range.push({min: state.allDates.start, max:state.allDates.end});
		if(state.context.os_niveau_id) {
			fetch('?graphql', {
				credentials: 'same-origin',
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					query: Betrekkingen,
					variables: {
						"financieel_context": {
							"os_niveau_id": os_niveau_id,
							"soort_id": soort_id,
							"jaar_periode_van": jaar_periode_van,
							"jaar_periode_tot": jaar_periiode_tot
						},
						"periodes": periodes,
						"total_range": total_range
					}
				})
			}).then(response => {
				return response.text().then(function(text) {
					return JSON.parse(text);
				});
			}).then(data => {
				//________________LOAD CONTRACTS_________________________________________________________________
				data.data.formatie.contracten.forEach(function(item){
					commit(mutations.STORE_CONTRACT, {personeelslid_id: item.personeelslid_id, contract: item});
				});
				commit(mutations.LOAD_CONTRACT);

				//________________LOAD BETREKKINGEN_________________________________________________________________
				let betrekkingen = [];
				data.data.formatie.betrekkingen_binnen_context.forEach(function(item){
					betrekkingen.push({data: item, binnen_os_selectie: 1});
				});
				data.data.formatie.betrekkingen_buiten_context.forEach(function(item){
					betrekkingen.push({data: item, binnen_os_selectie: 0});
				});
				betrekkingen.forEach(function (item){
					commit(mutations.STORE_BETREKKING, {personeelslid_id: item.data.personeelslid_id, betrekking: item});
				});
				commit(mutations.LOAD_BETREKKINGEN);
				commit(mutations.LOADER_OFF);
			}).catch(ex => {
				console.log(ex);
			});
		}
    },
    [resources.LOAD_CAOS]:({commit}) =>{
       Vue.resource('index.html?module=something&action=formatie&saction=caos').get().then(response => {
                    return response.json();
                }).then(data => {
                        data.forEach(function (item) {
                        commit(mutations.STORE_CAOS, item);
                        commit(mutations.STORE_SCHALEN, item);
                        commit(mutations.STORE_TREDEN,item);
                    });
                });
    },
    [resources.LOAD_KOSTENPLAATSEN]:({commit}) =>{
        Vue.resource("index.html?module=something&action=formatie&saction=kostenplaatsen").get().then(function(response) {
            response.data.forEach(function (item) {
                commit(mutations.STORE_KOSTENPLAATS, item);
            });
        });
    },
    [resources.LOAD_FUNCTIECATEGORIEN]:({commit}) =>{
        Vue.resource("index.html?module=something&action=formatie&saction=functiecategorien").get().then(function(response) {
            response.data.forEach(function (item) {
                commit(mutations.STORE_FUNCTIECATEGORIE, item);
            });
        });
    },
    [resources.TOGGLE_PERSONEELS_DETAIL_VIEW]:({commit, state}, {personeelslid_id, view}) =>{
        if (view === 'personeelslid-details-periodes' && !state.personeelsleden[state.personeels_map[personeelslid_id]].totalenMaandenLoaded){
            Vue.resource("index.html?module=something&action=formatie&saction=personeeltotalenmaanden&personeelslid_id={personeelslid_id}")
                .get({personeelslid_id: personeelslid_id}).then(function (response) {
                response.data.forEach(function (item) {
                    commit(mutations.STORE_PERSONEEL_TOTALEN_MAANDEN, {personeelslid_id: personeelslid_id, totalen: item});
                });
                commit(mutations.PERSONEEL_TOTALEN_MAANDEN_LOADED, personeelslid_id);
            })
        }
        commit(mutations.UPDATE_PERSONEELSLID_DETAIL_VIEW, {personeelslid_id: personeelslid_id, view: view});
    },
    [resources.TOGGLE_BETREKKINGEN]:({commit}, personeelslid_id) =>{
        commit(mutations.TOGGLE_BETREKKINGEN, personeelslid_id);
    },
    [resources.TOGGLE_EDIT_PERSONEELSLID]:({commit},personeelslid_id) =>{
        commit(mutations.RESET_EDIT_STATES, {except_personeelslid_id:personeelslid_id});
        commit(mutations.TOGGLE_EDIT_PERSONEELSLID, personeelslid_id);
    },
    [resources.TOGGLE_VIEW_PERSONEELSLID]:({commit}, personeelslid_id) =>{
        commit(mutations.RESET_DETAILVIEW_STATES, personeelslid_id);
        commit(mutations.TOGGLE_VIEW_PERSONEELSLID, personeelslid_id);
    },
    [resources.TOGGLE_EDIT_BETREKKING]:({commit}, {betrekking_id, personeelslid_id, contract_id}) =>{
        commit(mutations.RESET_EDIT_STATES, {except_personeelslid_id: null, except_betrekking_id: betrekking_id});
        commit(mutations.TOGGLE_EDIT_BETREKKING, {betrekking_id: betrekking_id, personeelslid_id: personeelslid_id, contract_id: contract_id});
    },
    [resources.UPDATE_DISPLAY_VALUE_TYPE]:({commit}, {type, show}) =>{
        commit(mutations.UPDATE_DISPLAY_VALUE_TYPE, {type: type, show: show});
    },
    [resources.SORT_FORMATIE]:({commit}, event) =>{
        commit(mutations.UPDATE_ACTIVE_SORT, event.target.value);
    }
}
