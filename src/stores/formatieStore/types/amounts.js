let amounts = {};
export function calculateAmounts(periodes, personeelslid){
	let amountPerPeriode = {};
	let totaalAmounts = {};
	periodes.forEach(function(period){
		amountPerPeriode[period.min] = {
			wtf: 0,
			werkgeverslasten: 0,
			brutoloon: 0,
			bapo: 0,
			tu: 0
		}
	});
	personeelslid.data.periodes.forEach(function(data){
		if(data){
			amountPerPeriode[data.periodrange.min] = {
				wtf: data.wtf,
				werkgeverslasten: data.werkgeverslasten,
				brutoloon: data.brutoloon,
				bapo: data.bapo,
				tu: data.tu
			}
		}
	});
	personeelslid.data.totaal.forEach(function(data){
		if(data){
			totaalAmounts[data.periodrange.max] = {
				wtf: data.wtf,
				werkgeverslasten: data.werkgeverslasten,
				brutoloon: data.brutoloon,
				bapo: data.bapo,
				tu: data.tu
			}
		}
	});
	amounts[personeelslid.data.id] = {amountPerPeriode: amountPerPeriode, totaalAmounts: totaalAmounts};
	return {amounts: amounts};
};