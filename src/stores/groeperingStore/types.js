//________GROEPERINGEN_TYPES________________________________________________________

//Getters
export const CURRENT_NIVEAU = 'tree/CURRENT_NIVEAU';
export const CURRENT_NIVEAU_CHILDREN = 'tree/CURRENT_NIVEAU_CHILDREN';
export const ONGEKKOPELDE_FINANCIEELE = 'tree/ONGEKKOPELDE_FINANCIEELE';
export const ONGEKKOPELDE_PERSONELE = 'tree/ONGEKKOPELDE_PERSONELE';
export const LOGS_LIST = 'tree/LOGS_LIST';
export const TREE = 'tree/TREE';
export const TREE_RENDER_MODE = 'tree/TREE_RENDER_MODE';
export const SELECTED_INDICES = 'tree/SELECTED_INDICES';
export const SELECTED_ITEM = 'tree/SELECTED_ITEM';
export const CREATED_OBJ = 'tree/CREATED_OBJ';
export const LAST_OBJ_CREATED = 'tree/LAST_OBJ_CREATED'; // adding the data to array of objects used by log file
export const LAST_SELECTED_ITEM = 'tree/LAST_SELECTED_ITEM';
export const ORIGIN_OBJ = 'tree/ORIGIN_OBJ';
export const ERRORS = 'tree/ERRORS';

//Mutations
export const MUTATE_CURRENT_NIVEAU = 'tree/MUTATE_CURRENT_NIVEAU';
export const MUTATE_ONGEKKOPELDE = 'tree/MUTATE_ONGEKKOPELDE';
export const MUTATE_UPDATE_LIST = 'tree/MUTATE_UPDATED_GEKKOPELDE_LIST';
export const MUTATE_UPDATE_LIST_LEVELS = 'tree/MUTATE_UPDATE_LIST_LEVELS';
export const MUTATE_NEXT_LEVEL = 'tree/MUTATE_NEXT_LEVEL';
export const MUTATE_RENDER_TREE = 'tree/MUTATE_RENDER_TREE';
export const MUTATE_TREE_RENDER_MODE = 'tree/MUTATE_TREE_RENDER_MODE';
export const MUTATE_SELECTED_ITEM_CHANGED = 'tree/MUTATE_SELECTED_ITEM_CHANGED';
export const MUTATE_UNDO_LAST_LINKING = 'tree/MUTATE_UNDO_LAST_LINKING';
export const MUTATE_ORIGIN_OBJ = 'tree/MUTATE_ORIGIN_OBJ';
export const MUTATE_ERRORS = 'tree/MUTATE_ERRORS';

//Actions
export const ACTION_CURRENT_NIVEAU = 'tree/ACTION_CURRENT_NIVEAU';
export const ACTION_ONGEKKOPELDE = 'tree/ACTION_ONGEKKOPELDE';
export const ACTION_UPDATE_LIST = 'tree/ACTION_UPDATED_GEKKOPELDE_LIST';
export const ACTION_UPDATE_LIST_LEVELS = 'tree/ACTION_UPDATE_LIST_LEVELS';
export const ACTION_NEXT_LEVEL = 'tree/ACTION_NEXT_LEVEL';
export const ACTION_RENDER_TREE = 'tree/ACTION_RENDER_TREE';
export const ACTION_TREE_RENDER_MODE = 'tree/ACTION_TREE_RENDER_MODE';
export const ACTION_SELECTED_ITEM_CHANGED = 'tree/ACTION_SELECTED_ITEM_CHANGED';
export const ACTION_UPDATE_DB = 'tree/ACTION_UPDATE_DB';
export const ACTION_UNDO_LAST_LINKING = 'tree/ACTION_UNDO_LAST_LINKING';
export const ACTION_ORIGIN_OBJ = 'tree/ACTION_ORIGIN_OBJ';


