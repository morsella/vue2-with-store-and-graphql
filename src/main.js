import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource';
import store from './stores/groeperingStore/store';
import 'whatwg-fetch';

Vue.use(VueResource);

new Vue({
    el: '#test',
    store,
    render: h => h(App)
});
