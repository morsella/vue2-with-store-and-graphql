import * as types from '../../types';

export default{
    [types.MUTATE_CURRENT_NIVEAU]:(state, payload) => {
        //Tree render based on current Niveau
        state.currentNiveau = payload;
        state.selected_item = state.currentNiveau;

        //Get Groeperings
        for(let i=0; i<state.selected_item.financieel_groeperingen.length; i++){
            state.selected_item.financieel_groeperingen[i].naam = 'financieel_groeperingen';
        }
        for(let i=0; i<state.selected_item.personeel_groeperingen.length; i++){
            state.selected_item.personeel_groeperingen[i].naam = 'personeel_groeperingen';
        }
    },
    [types.MUTATE_RENDER_TREE]:(state, payload) => { // store_tree
        state.tree = payload;
    },
    [types.MUTATE_TREE_RENDER_MODE]:(state, render_mode) => {
        if(render_mode !== 'financieel_groeperingen' && render_mode !== 'personeel_groeperingen') {
            render_mode = 'normaal';
        }
        state.tree_render_mode = render_mode;
    },
    [types.MUTATE_NEXT_LEVEL]:(state, payload) =>{
        if (state.selected_indices[payload.level] !== payload.index) {
            state.selected_indices.splice(payload.level, state.selected_indices.length, payload.index, 0);
        }
    },
    [types.MUTATE_SELECTED_ITEM_CHANGED]:(state, payload) =>{
        state.selected_item = payload;
        for(let i=0; i<state.selected_item.financieel_groeperingen.length; i++){
            state.selected_item.financieel_groeperingen[i].naam = 'financieel_groeperingen';
        }
        for(let i=0; i<state.selected_item.personeel_groeperingen.length; i++){
            state.selected_item.personeel_groeperingen[i].naam = 'personeel_groeperingen';
        }

    },
    [types.MUTATE_ONGEKKOPELDE]: (state, payload) => {
        state.ongekkopeldeFinancieele = payload.ongekoppelde_financiele_groeperingen;
        state.ongekkopeldePersonele = payload.ongekoppelde_personele_groeperingen;
        for(let i =0; i < state.ongekkopeldePersonele.length; i++){
            state.ongekkopeldePersonele[i].naam = 'personeel_groeperingen';
        }
        for(let i =0; i < state.ongekkopeldeFinancieele.length; i++){
            state.ongekkopeldeFinancieele[i].naam = 'financieel_groeperingen';
        }
    },
    [types.MUTATE_UPDATE_LIST]: (state, payload)=>{
        let groepering;
        let log = {};
        console.log('list');
        console.log(payload);
        let reduce = function(carry, obj) {
            if(parseInt(obj.id) !== parseInt(payload.id)) {
                carry.push(obj);
            } else {
                groepering = obj;
                log = {
                    id: payload.id,
                    groepering: groepering,
                    to: (payload.list_to_link_to === 'linked'? (state.selected_item.parent? state.selected_item.parent.naam + ' > ': '' )+state.selected_item.naam : ' Ongekoppelde Groeperingen' ),
                    from: (payload.list_to_link_to === 'linked'? ' Ongekoppelde Groeperingen': (state.selected_item.parent? state.selected_item.parent.naam+ ' > ': '' )+state.selected_item.naam)
                }
            }
            return carry
        };

        if (payload.list_to_link_to === 'linked') {
                if (payload.type === 'personeel_groeperingen'  && state.selected_item.accepteert_personeel_groeperingen.boolean === true ) {
                    state.ongekkopeldePersonele = state.ongekkopeldePersonele.reduce(reduce, []);
                   if(groepering){
                        state.selected_item.personeel_groeperingen.push(groepering);
                        state.logs.splice(0, 0, log);
                        console.log('not synchronous?');
                       state.created_obj = { action: 'link', targetId: state.selected_item.id, groeperingId: payload.id, type: payload.type };
                       state.last_selected_item = state.selected_item;
                    }
                }else if(payload.type === 'personeel_groeperingen' && state.selected_item.accepteert_personeel_groeperingen.boolean === false) {
                    alert('U kunt hier geen groeperingen koppelen');
                }
                if (payload.type === 'financieel_groeperingen' && state.selected_item.accepteert_financieel_groeperingen.boolean === true) {
                    state.ongekkopeldeFinancieele = state.ongekkopeldeFinancieele.reduce(reduce, []);
                    if(groepering){
                        state.selected_item.financieel_groeperingen.push(groepering);
                        state.logs.splice(0, 0, log);
                        state.created_obj = { action: 'link', targetId: state.selected_item.id, groeperingId: payload.id, type: payload.type };
                        state.last_selected_item = state.selected_item;
                    }
                }else if(payload.type === 'financieel_groeperingen' && state.selected_item.accepteert_financieel_groeperingen.boolean === false){
                    alert('U kunt hier geen groeperingen koppelen');
                }
        } else {
            if(payload.type === 'personeel_groeperingen'){
                state.selected_item.personeel_groeperingen = state.selected_item.personeel_groeperingen.reduce(reduce, []);
                    if (groepering) {
                        state.ongekkopeldePersonele.push(groepering);
                        state.logs.splice(0, 0, log);
                    }
            }
            if(payload.type === 'financieel_groeperingen'){
                state.selected_item.financieel_groeperingen = state.selected_item.financieel_groeperingen.reduce(reduce, []);
                if (groepering) {
                    state.ongekkopeldeFinancieele.push(groepering);
                    state.logs.splice(0, 0, log);
                }

            }
            state.created_obj = { action: 'unlink', targetId: state.selected_item.id, groeperingId: payload.id, type: payload.type };
        }
        state.last_obj_created.splice(0,0,{dropped_obj: payload, origin_obj: state.origin_obj, last_selected: state.last_selected_item});
    },
    [types.MUTATE_UPDATE_LIST_LEVELS]:(state, payload)=> {
        let groepering;
        let log = {};
        console.log('levels');
        console.log(payload);
        let reduce = function (carry, obj) {
            if (parseInt(obj.id) !== parseInt(payload.id)) {
                carry.push(obj);
            } else {
                groepering = obj;
                log = {
                    id: payload.id,
                    groepering: groepering,
                    to: payload.destination.parent.naam+ ' > ' +payload.destination.naam,
                    from: (payload.origin === 'ongekkopelde_personeele' || payload.origin === 'ongekkopelde_fenancieele' ? ' Ongekoppelde Groeperingen': (state.selected_item.parent? state.selected_item.parent.naam+ ' > ': '' )+state.selected_item.naam)
                }
            }
            return carry
        };
        if (payload.origin === 'ongekkopelde_personeele' && payload.type === 'personeel_groeperingen') {
            state.ongekkopeldePersonele = state.ongekkopeldePersonele.reduce(reduce, []);
            if (groepering) {
                payload.destination.personeel_groeperingen.push(groepering);
                state.logs.splice(0, 0, log);
            }
        }else if (payload.origin === 'ongekkopelde_fenancieele' && payload.type === 'financieel_groeperingen'){
            state.ongekkopeldeFinancieele = state.ongekkopeldeFinancieele.reduce(reduce, []);
            if (groepering){
                payload.destination.financieel_groeperingen.push(groepering);
                state.logs.splice(0, 0, log);
            }
        }
        if(payload.origin === state.selected_item.naam) {
            if (payload.type === 'financieel_groeperingen') {
                state.selected_item.financieel_groeperingen = state.selected_item.financieel_groeperingen.reduce(reduce, []);
                if (groepering) {
                    payload.destination.financieel_groeperingen.push(groepering);
                    state.logs.splice(0, 0, log);
                }
            }else if (payload.type === 'personeel_groeperingen') {
                state.selected_item.personeel_groeperingen = state.selected_item.personeel_groeperingen.reduce(reduce, []);
                if (groepering) {
                    payload.destination.personeel_groeperingen.push(groepering);
                    state.logs.splice(0, 0, log);
                }
            }
        }
        state.created_obj = { action: 'link', targetId: payload.destination.id, groeperingId: payload.id, type: payload.type };
        state.last_obj_created.splice(0,0,{dropped_obj: payload, origin_obj: state.origin_obj, last_selected: state.last_selected_item});
    },
    [types.MUTATE_ORIGIN_OBJ]:(state, payload)=>{
        state.origin_obj = payload;
    },
    [types.MUTATE_UNDO_LAST_LINKING]: (state, payload) =>{
        let groepering;
        let reduce = function(carry, obj){
            if(parseInt(obj.id) !== parseInt(payload.dropped_obj.id)){
                carry.push(obj);
            }else{
             groepering = obj;
            }
             return carry
            };
        console.log('undo from mutation');
        if(payload.origin_obj === 'ongekkopelde'){
            if(payload.dropped_obj.destination){
                if(payload.dropped_obj.type === 'personeel_groeperingen') {
                    payload.dropped_obj.destination.personeel_groeperingen =  payload.dropped_obj.destination.personeel_groeperingen.reduce(reduce, []);
                    if (groepering) {
                        state.ongekkopeldePersonele.push(groepering);
                    }
                }else{
                    payload.dropped_obj.destination.financieel_groeperingen =  payload.dropped_obj.destination.financieel_groeperingen.reduce(reduce, []);
                    if(groepering){
                        state.ongekkopeldeFinancieele.push(groepering);
                    }
                }
                state.created_obj = { action: 'unlink', targetId: payload.dropped_obj.destination.id, groeperingId: payload.dropped_obj.id, type: payload.dropped_obj.type };
            }else{
                if(payload.dropped_obj.type === 'personeel_groeperingen'){
                    payload.last_selected.personeel_groeperingen = payload.last_selected.personeel_groeperingen.reduce(reduce, []);
                    if(groepering){
                        state.ongekkopeldePersonele.push(groepering);
                    }
                }else{
                    payload.last_selected.financieel_groeperingen = payload.last_selected.financieel_groeperingen.reduce(reduce, []);
                    if(groepering){
                        state.ongekkopeldeFinancieele.push(groepering);
                    }
                }
                state.created_obj = { action: 'unlink', targetId: payload.last_selected.id, groeperingId: payload.dropped_obj.id, type: payload.dropped_obj.type };
            }
        }else{
            if(payload.dropped_obj.destination){
                if(payload.dropped_obj.type === 'personeel_groeperingen') {
                    payload.dropped_obj.destination.personeel_groeperingen =  payload.dropped_obj.destination.personeel_groeperingen.reduce(reduce, []);
                    if (groepering) {
                        payload.origin_obj.personeel_groeperingen.push(groepering);
                    }
                }else{
                    payload.dropped_obj.destination.financieel_groeperingen =  payload.dropped_obj.destination.financieel_groeperingen.reduce(reduce, []);
                    if(groepering){
                        payload.origin_obj.financieel_groeperingen.push(groepering);
                    }
                }
                state.created_obj = { action: 'link', targetId: payload.origin_obj.id, groeperingId: payload.dropped_obj.id, type: payload.dropped_obj.type };
            }else{
                if(payload.dropped_obj.type === 'personeel_groeperingen') {
                    state.ongekkopeldePersonele = state.ongekkopeldePersonele.reduce(reduce, []);
                    if(groepering){
                        payload.origin_obj.personeel_groeperingen.push(groepering);
                    }
                }else{
                    state.ongekkopeldeFinancieele = state.ongekkopeldeFinancieele.reduce(reduce, []);
                    if(groepering){
                        payload.origin_obj.financieel_groeperingen.push(groepering);
                    }
                }
                state.created_obj = { action: 'link', targetId: payload.origin_obj.id, groeperingId:  payload.dropped_obj.id, type:  payload.dropped_obj.type };
            }
        }
        state.logs.splice(0, 1);
        state.last_obj_created.splice(0, 1);
    },
    [types.MUTATE_ERRORS]:(state, payload) =>{
        state.errors = payload;
    }
}
