function deepClone(object) { return JSON.parse(JSON.stringify(object)); }

export function byName(personeelsleden) {
    return deepClone(personeelsleden).sort(function (a, b) {
        if (a.data.naam < b.data.naam)
            return -1;
        if (a.data.naam > b.data.naam)
            return 1;
        return 0;
    });
};
export function byPersoneelsnummer(personeelsleden) {
    return deepClone(personeelsleden).sort(function(a, b) {
        if (a.data.personeelsnummer < b.data.personeelsnummer)
            return -1;
        if (a.data.personeelsnummer > b.data.personeelsnummer)
            return 1;
        return 0;
    });
};
// export function byWtf(personeelsleden) {
//     return deepClone(personeelsleden).sort(function(a, b) {
//         if (parseFloat(a.data[("wtf" + state.laagste_jaar)] + '.' + a.data[("wtf" + (state.laagste_jaar + 1))]) < parseFloat(b.data[("wtf" + state.laagste_jaar)] + '.' + b.data[("wtf" + (state.laagste_jaar + 1))]))
//             return -1;
//         if (parseFloat(a.data[("wtf" + state.laagste_jaar)] + '.' + a.data[("wtf" + (state.laagste_jaar + 1))]) > parseFloat(b.data[("wtf" + state.laagste_jaar)] + '.' + b.data[("wtf" + (state.laagste_jaar + 1))]))
//             return 1;
//         return 0;
//     });
// };
// export function byTu(personeelsleden) {
//     return deepClone(personeelsleden).sort(function(a, b) {
//         if (!a.data[("tu" + state.laagste_jaar)] || parseFloat(a.data[("tu" + state.laagste_jaar)] + '.' + a.data[("tu" + (state.laagste_jaar + 1))]) < parseFloat(b.data[("tu" + state.laagste_jaar)] + '.' + b.data[("tu" + (state.laagste_jaar + 1))]))
//             return -1;
//         if (!b.data[("tu" + state.laagste_jaar)] || parseFloat(a.data[("tu" + state.laagste_jaar)] + '.' + a.data[("tu" + (state.laagste_jaar + 1))]) > parseFloat(b.data[("tu" + state.laagste_jaar)] + '.' + b.data[("tu" + (state.laagste_jaar + 1))]))
//             return 1;
//         return 0;
//     });
// };
// export function byBapo(personeelsleden) {
//     return deepClone(personeelsleden).sort(function(a, b) {
//         if (!a.data[("bapo" + state.laagste_jaar)] || parseFloat(a.data[("bapo" + state.laagste_jaar)] + '.' + a.data[("bapo" + (state.laagste_jaar + 1))]) < parseFloat(b.data[("bapo" + state.laagste_jaar)] + '.' + b.data[("bapo" + (state.laagste_jaar + 1))]))
//             return -1;
//         if (!b.data[("bapo" + state.laagste_jaar)] || parseFloat(a.data[("bapo" + state.laagste_jaar)] + '.' + a.data[("bapo" + (state.laagste_jaar + 1))]) > parseFloat(b.data[("bapo" + state.laagste_jaar)] + '.' + b.data[("bapo" + (state.laagste_jaar + 1))]))
//             return 1;
//         return 0;
//     });
// };
export function byFunctiecategorie(personeelsleden) {
    return deepClone(personeelsleden).sort(function(a, b) {
        if (a.functiesCodes < b.functiesCodes)
            return -1;
        if (a.functiesCodes > b.functiesCodes)
            return 1;
        return 0;
    });
};
// export function byWerkgeverslasten(personeelsleden) {
//     return deepClone(personeelsleden).sort(function(a, b) {
//         if (parseFloat(a.data[("wl" + state.laagste_jaar)] + '.' + a.data[("wl" + (state.laagste_jaar + 1))]) < parseFloat(b.data[("wl" + state.laagste_jaar)] + '.' + b.data[("wl" + (state.laagste_jaar + 1))]))
//             return -1;
//         if (parseFloat(a.data[("wl" + state.laagste_jaar)] + '.' + a.data[("wl" + (state.laagste_jaar + 1))]) > parseFloat(b.data[("wl" + state.laagste_jaar)] + '.' + b.data[("wl" + (state.laagste_jaar + 1))]))
//             return 1;
//         return 0;
//     });
// };
// export function byBrutoloon(personeelsleden) {
//     return deepClone(personeelsleden).sort(function(a, b) {
//         if (parseFloat(a.data[("bl" + state.laagste_jaar)] + '.' + a.data[("bl" + (state.laagste_jaar + 1))]) < parseFloat(b.data[("bl" + state.laagste_jaar)] + '.' + b.data[("bl" + (state.laagste_jaar + 1))]))
//             return -1;
//         if (parseFloat(a.data[("bl" + state.laagste_jaar)] + '.' + a.data[("bl" + (state.laagste_jaar + 1))]) > parseFloat(b.data[("bl" + state.laagste_jaar)] + '.' + b.data[("bl" + (state.laagste_jaar + 1))]))
//             return 1;
//         return 0;
//     });
// };
export function byAantalBetrekkingen(personeelsleden) {
    return deepClone(personeelsleden).sort(function(a, b) {
        if (parseFloat(a.betrekkingen_total.length + '.' + a.betrekking_current.length) < parseFloat(b.betrekkingen_total.length + '.' + b.betrekking_current.length))
            return -1;
        if (parseFloat(a.betrekkingen_total.length + '.' + a.betrekking_current.length) > parseFloat(b.betrekkingen_total.length + '.' + b.betrekking_current.length))
            return 1;
        return 0;
    });
};