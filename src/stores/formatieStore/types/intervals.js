let periode_100_to_12 = function(jp) {
	let j = Math.floor(jp / 100);
	let p = Math.floor(jp % 100);
	return Math.floor(j*12 + p - 1);
};
let periode_12_to_100 = function(jp) {
	let j = Math.floor(jp / 12);
	let p = Math.floor(jp % 12);
	return Math.floor(j*100 + p + 1);
};

export function byPeriodes(dates, interval) {
		let jp_start = periode_100_to_12(dates.start);
		let jp_end = periode_100_to_12(dates.end);

		let tmp_dates = [];
		for ( let i = jp_start, x=0; i<=jp_end; i++) {
			let number = Math.floor(x++/interval);
			if (typeof tmp_dates[number] === 'undefined') {
				tmp_dates[number] = [];
			}
			tmp_dates[number].push(periode_12_to_100(i));
		}
		let periodesArray = [];
		let periodesArray2 = [];
		let jaren = [];
		for(let val of tmp_dates){
			jaren.push(Math.floor(val[0] / 100));
			periodesArray.push(Math.floor(val[0] / 100) + '-' + Math.floor(val[0] % 100)
				+ ' \u21D4 '+ Math.floor(val[(val.length - 1)] / 100) + '-' + Math.floor(val[(val.length - 1)] % 100));

			periodesArray2.push({
				start:Math.floor(val[0] / 100) + '-' + Math.floor(val[0] % 100),
				end: Math.floor(val[(val.length - 1)] / 100) + '-' + Math.floor(val[(val.length - 1)] % 100)
			});


		}
	return {periodesHtml: periodesArray, periodesHtml2: periodesArray2, jaren: jaren, periodes: tmp_dates};
};
