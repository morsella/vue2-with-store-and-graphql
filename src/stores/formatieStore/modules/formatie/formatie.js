import * as type_getters from '../../types/getters';
import mutations from './mutations';
import actions from './actions';
import * as sort from "../../types/sort";
import * as interval from "../../types/intervals";


const state = {
    context: {},
    personeels_map: {},
    contracten_map: {},
    betrekkingen_map: {},
    personeelsleden: [],
    jaren: [],
    laagste_jaar: 0,
    hoogste_jaar: 0,
    caos: {},
    selected_cao_costs: 0,
    kostenplaatsen: {},
    functiecategorien: {},
    display_value_type: {wtf: false, tu: false, brutoloon: false, werkgeverslasten: true, bapo: false},
    show_modal: false,
    modalComponent: "modal-dates",
    modalData: {},
    activeSortIndex: 0,
    sortOptions: {
        0: {label: "Op naam", sortFunction: sort.byName},
        1: {label: "Op personeelsnummer", sortFunction: sort.byPersoneelsnummer},
        // 2: {label: "Op WTF", sortFunction: sort.byWtf},
        // 3: {label: "Op TU", sortFunction: sort.byTu},
        // 4: {label: "Op BAPO", sortFunction: sort.byBapo},
        5: {label: "Op functiecategorie", sortFunction: sort.byFunctiecategorie},
        // 6: {label: "Op werkgeverslasten", sortFunction: sort.byWerkgeverslasten},
        // 7: {label: "Op brutoloon", sortFunction: sort.byBrutoloon},
        8: {label: "Op aantal betrekkingen", sortFunction: sort.byAantalBetrekkingen}
    },
    activeIntervalVal: 0,
    intervals: {
    	0: {label: "12 maanden", val: 12, intervalFunction: interval.byPeriodes},
		1: {label: "9 maanden", val: 9, intervalFunction: interval.byPeriodes},
		2: {label: "6 maanden", val: 6, intervalFunction: interval.byPeriodes},
		3: {label: "3 maanden", val: 3, intervalFunction: interval.byPeriodes},
		4: {label: "1 maand", val: 1, intervalFunction: interval.byPeriodes}
	},
	allDates: {},
	periodes_array: [],
    loader: false,
    amountsPersonelsleden: []


};
const getters = {
    [type_getters.PERSONEELS_MAP]: state =>{
        return state.personeels_map;
    },
    [type_getters.BETREKKINGEN_MAP]: state =>{
        return state.betrekkingen_map;
    },
    [type_getters.PERSONEELSLEDEN]: state =>{
        return state.personeelsleden;
    },
    [type_getters.JAREN]: state =>{
        return state.jaren;
    },
    [type_getters.LAAGSTE_JAAR]: state =>{
        return state.laagste_jaar;
    },
    [type_getters.HOOGSTE_JAAR]: state =>{
        return state.hoogste_jaar;
    },
    [type_getters.CAOS]: state =>{
        return state.caos;
    },
    [type_getters.SELECTED_CAO_COSTS]: state =>{
        return state.selected_cao_costs;
    },
    [type_getters.KOSTENPLAATSEN]: state =>{
        return state.kostenplaatsen;
    },
    [type_getters.FUNCTIECATEGORIEN]: state =>{
        return state.functiecategorien;
    },
    [type_getters.DISPLAY_VALUE_TYPE]: state =>{
        return state.display_value_type;
    },
    [type_getters.SHOW_MODAL]: state =>{
        return state.show_modal;
    },
    [type_getters.MODAL_COMPONENT]: state =>{
        return state.modalComponent;
    },
    [type_getters.MODALDATA]: state =>{
        return state.modalData;
    },
    [type_getters.ACTIVESORT]: state =>{
        return state.sortOptions[state.activeSortIndex];
    },
    [type_getters.SORT_OPTIONS]: state =>{
        return state.sortOptions;
    },
    [type_getters.CONTEXT]: state =>{
        return state.context;
    },
    [type_getters.INTERVALS]: state =>{
        return state.intervals;
    },
    [type_getters.ACTIVE_INTERVAL]:state =>{
        return state.intervals[state.activeIntervalVal];
    },
	[type_getters.ALL_DATES]:state =>{
    	return state.allDates;
	},
	[type_getters.PERIODES_ARRAY]: state =>{
    	return state.periodes_array;
	},
    [type_getters.CONTRACTEN_MAP]: state =>{
        return state.contracten_map;
    },
    [type_getters.LOADER]:state =>{
        return state.loader;
    },
    [type_getters.AMOUNTS_PERSONEELSLEDEN]:state =>{
        return state.amountsPersonelsleden;
    }

};

export default{
    state,
    getters,
    mutations,
    actions
}


